
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

// Page URL:https://avic.ua/

public class AvicTest extends PageObject{
    private final String LOGIN_MAIL = "mail@example.com";
    private final String PASSWORD = "Last Name";
    private final String PHONE_MODEL = "Samsung Galaxy";

    @FindBy(name = "login")
    private WebElement loginName;

    @FindBy(name = "password")
    private WebElement password;

    @FindBy(xpath = "//button[contains(@class, \"main-btn submit\")]")
    private WebElement loginButton;


    @FindBy(xpath = "//input[@id=\"input_search\"]")
    private WebElement inputSamsungGalaxy;

    @FindBy(xpath = "//button[contains(@class,\"button-reset search-btn\")]")
    private WebElement searchButton;


    public AvicTest(WebDriver driver) {
        super(driver);
    }

    public void enterLogin(){
        this.loginName.sendKeys(LOGIN_MAIL);
    }
    public void enterPassword(){
        this.password.sendKeys(PASSWORD);
    }
    public void pressLoginButton(){
        this.loginButton.click();
    }
   // public void verifyAlertSuccess(){
   //     this.alertSuccess.isDisplayed();
    //}

    public void enterPhoneModel(){
        this.inputSamsungGalaxy.sendKeys(PHONE_MODEL);
    }
    public void pressSearchButton() { this.searchButton.click(); }
}
