import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.Point;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.openqa.selenium.By.xpath;
import static org.testng.Assert.assertTrue;

public class TestPlan {
    private static WebDriver driver;// = new ChromeDriver();


    @BeforeSuite
    public static void main(String[] args) {
        // ChromeDriver location set up in Utils class
        System.setProperty("webdriver.chrome.driver", Utils.CHROME_DRIVER_LOCATION);
    }

    @BeforeMethod
    public  void testSetUp(){
        driver = new ChromeDriver();
        Point p = new Point(0,3000);
        driver.manage().window().setPosition(p);//открыли браузер на весь экран
        driver.get(Utils.BASE_URL);
    }

    @Test(testName = "Loging Page", priority = 1)
    public static void checkSubmitLoginPage(){
        driver.get(Utils.SingIn_URL);
        AvicTest avicLoginForm = new AvicTest(driver);
        avicLoginForm.enterLogin();
        avicLoginForm.enterPassword();
        avicLoginForm.pressLoginButton();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //avicLoginForm.verifyAlertSuccess();
    }
    @Test(testName = "SamsungGalaxy", priority = 2)
    public static void checkThatUrlContainsSamsungGalaxy(){
        driver.get(Utils.Telefony_URL); //open smartfony page
        AvicTest avicSamsungGalaxy = new AvicTest(driver);
        avicSamsungGalaxy.enterPhoneModel();
        avicSamsungGalaxy.pressSearchButton();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        assertTrue(driver.getCurrentUrl().contains("query=Samsung+Galaxy"));//проверяем что урла содержит кверю
    }
    @Test(testName = "makeSamsungGalaxyList", priority = 3)
    public static void checkThatSearchResultsContainsSearchWord(){
        driver.get(Utils.Telefony_URL); //open smartfony page
        AvicTest avicSamsungGalaxy = new AvicTest(driver);
        avicSamsungGalaxy.enterPhoneModel();
        avicSamsungGalaxy.pressSearchButton();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        List<WebElement> elementList = driver.findElements(xpath("//div[@class='prod-cart__descr']"));
        for (WebElement webElement : elementList) {
            assertTrue(webElement.getText().contains("Samsung Galaxy"));
        }
    }


    @AfterSuite
    public static void cleanUp(){
        driver.manage().deleteAllCookies();
        driver.close();
    }
}
